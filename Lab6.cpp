﻿#include <Header.hpp>
#include <iostream>
#include <vector>
void Read(int &n, int &m, int mat[100][100])
{
    std::cin >> n >> m;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            std::cin >> mat[i][j];
        }
    }

}
int main()
{
    int n, m;
    int mat[100][100];
    Read(n, m, mat);
    if (mt::ConsistPrime(n, m, mat) && mt::Analiz(n, m, mat))
        mt::sort(n, m, mat);
    mt::Write(n, m, mat);

}

