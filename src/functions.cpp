#include <Header.hpp>
#include <iostream>
#include <math.h>
#include <vector>

namespace mt {
    bool Analiz(int n, int m, int mat[100][100]) {
        int k = 0;
        for (int j = 0; j < m - 1; j++) {
            for (int j1 = j + 1; j1 < m; j1++) {
                k = 0;
                for (int i = 0; i < n; i++) {
                    if (mat[i][j] == mat[i][j1]) {
                        k++;
                    }
                    else break;
                }

                if (k == n) {
                    return true;
                }
            }
        }
        return false;
    }

    bool isPrime(int x) {
        if (abs(x) < 2)
            return false;
        for (int d = 2; d <= sqrt(abs(x)); d++)
            if (x % d == 0)
                return false;
        return true;
    }

    bool  ConsistPrime(int n, int m, int mat[100][100]) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (isPrime(mat[i][j])) {
                    return true;
                }
            }
        }
        return false;
    }

    void swap(int& a, int& b) {
        int tmp = a;
        a = b;
        b = tmp;

    }

    int sum(int a, int b, int mat[100][100]) {
        int sum = 0;
        for (int j = 0; j < b; j++) {
            sum = sum + abs(mat[a][j]);
        }
        return sum;
    }

    void sort(int n, int m, int mat[100][100]) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (sum(i, m, mat) > sum(j, m, mat)) {
                    for (int k = 0; k < m; k++) {
                        swap(mat[i][k], mat[j][k]);
                    }
                }
            }

        }
    }

    void Write(int n, int m, int mat[100][100])
    {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                std::cout << mat[i][j] << " ";
            }
            std::cout << std::endl;
        }
    }


}

